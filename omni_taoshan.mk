# Get the full APNs list
$(call inherit-product, vendor/omni/config/gsm.mk)

# Inherit from omni custom product configuration
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base_telephony.mk)

#Boot Animation
TARGET_SCREEN_HEIGHT := 854
TARGET_SCREEN_WIDTH := 480

$(call inherit-product, device/sony/taoshan/taoshan.mk)

PRODUCT_DEVICE := taoshan
PRODUCT_NAME := omni_taoshan
PRODUCT_BRAND := Sony
PRODUCT_MODEL := C2105
PRODUCT_MANUFACTURER := Sony
PRODUCT_CHARACTERISTICS := phone
