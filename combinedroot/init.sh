#!/sbin/busybox sh
#Boot Script

set +x
_PATH="$PATH"
export PATH=/sbin
busybox cd /
busybox date >>TKP-boot.txt
exec >>TKP-boot.txt 2>&1
busybox rm /init

# include device specific vars
source /sbin/bootrec-device

# create directories
busybox mkdir -m 755 -p /cache
busybox mkdir -m 755 -p /dev/block
busybox mkdir -m 755 -p /dev/input
busybox mkdir -m 555 -p /proc
busybox mkdir -m 755 -p /sys

# create device nodes
busybox mknod -m 600 /dev/block/mmcblk0 b 179 0
busybox mknod -m 600 ${BOOTREC_CACHE_NODE}
busybox mknod -m 600 ${BOOTREC_EVENT_NODE}
busybox mknod -m 666 /dev/null c 1 3

# mount filesystems
busybox mount -t proc proc /proc
busybox mount -t sysfs sysfs /sys
busybox mount -t ext4 ${BOOTREC_CACHE} /cache

# trigger amber LED
busybox echo 255 > ${BOOTREC_LED_RED}
busybox echo 255 > ${BOOTREC_LED_GREEN}
busybox echo 255 > ${BOOTREC_LED_BLUE}

# keycheck
busybox cat ${BOOTREC_EVENT} > /dev/keycheck&
busybox sleep 1 #remove unnecessary delay

# Turn off LED
busybox echo 0 > ${BOOTREC_LED_RED}
busybox echo 0 > ${BOOTREC_LED_GREEN}
busybox echo 0 > ${BOOTREC_LED_BLUE}

# Load ramdisk
load_image=/sbin/ramdisk.cpio.xz #we need xz format for booting TKP

# boot decision
if [ -s /dev/keycheck ] || busybox grep -q warmboot=0x77665502 /proc/cmdline ; then
	busybox echo 'recovery BOOT' >>TKP-boot.txt
	busybox rm -f /cache/recovery/boot
	# Cyan led for recovery boot
        busybox echo 0 > /sys/module/msm_fb/parameters/align_buffer
	busybox echo 0 > ${BOOTREC_LED_RED}
	busybox echo 255 > ${BOOTREC_LED_GREEN}
	busybox echo 255 > ${BOOTREC_LED_BLUE}
        busybox echo 100 > ${BOOTREC_VIBRATOR}
	# recovery ramdisk
	load_image=/sbin/recovery.cpio.xz
else
	busybox echo 'ANDROID BOOT' >>TKP-boot.txt
	# poweroff LED
	busybox echo 0 > ${BOOTREC_LED_RED}
	busybox echo 0 > ${BOOTREC_LED_GREEN}
	busybox echo 0 > ${BOOTREC_LED_BLUE}
fi

# kill the keycheck processes
busybox pkill -f "busybox cat ${BOOTREC_EVENT}"

# unpack the ramdisk image
busybox xzcat ${load_image} | busybox cpio -i

busybox umount /cache
busybox umount /proc
busybox umount /sys

busybox rm -fr /dev/*
busybox date >>TKP-boot.txt
export PATH="${_PATH}"
exec /init
